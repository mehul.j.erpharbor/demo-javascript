# -*- coding: utf-8 -*-
from odoo import http

class DemoJs(http.Controller):
    @http.route('/demo_js/demo_js/', auth='public')
    def index(self, **kw):
        return "Hello, world"

    @http.route('/demo_js/demo_js/objects/', auth='public')
    def list(self, **kw):
        return http.request.render('demo_js.listing', {
            'root': '/demo_js/demo_js',
            'objects': http.request.env['demo_js.demo_js'].search([]),
        })

    @http.route('/demo_js/demo_js/objects/<model("demo_js.demo_js"):obj>/', auth='public')
    def object(self, obj, **kw):
        return http.request.render('demo_js.object', {
            'object': obj
        })