# -*- coding: utf-8 -*-
from odoo import models, fields, api

class demo_js(models.Model):
    _name = 'demo_js.demo_js'

    name = fields.Char()
    value = fields.Integer()
    description = fields.Text()

    # @api.model
    # def my_func(self):
    #     print("HEllo..!")
